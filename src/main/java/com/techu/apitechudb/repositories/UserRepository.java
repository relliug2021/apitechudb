package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel,String> {
    @Query(value="{}",sort = "{name : 1}")
    List<UserModel> userOrderBy(@Param("sort") String sort);
}
