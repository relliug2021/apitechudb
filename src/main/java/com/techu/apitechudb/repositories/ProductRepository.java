package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel,String> { //las interface facilitan la herencia horizontal para homogenizar que clases heterogeneas tenga algunos comportamientos iguales


}
