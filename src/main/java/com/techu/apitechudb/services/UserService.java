package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");
        return this.userRepository.findAll();
    }
    public List<UserModel> findAllSorted(String sort_type) {
        System.out.println("findAll en findAllSorted");
        return this.userRepository.userOrderBy(sort_type);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("add User en UserService");
        return this.userRepository.save(user); //save es un upsert (update or insert)

    }

    public UserModel updateUser(UserModel user) {
        System.out.println("Update User en UserService");
        return this.userRepository.save(user); //save es un upsert (update or insert)
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("User a borrar encontrado");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}

