package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel addProduct(ProductModel product){
        System.out.println("add product en ProductoService");
        return this.productRepository.save(product); //save es un upsert (update or insert)

    }

    public ProductModel updateProduct(ProductModel product){
        System.out.println("UpdateProduct en ProductService");
        return this.productRepository.save(product); //save es un upsert (update or insert)
    }

    public boolean delete(String id){
        System.out.println("delete en ProductoService");
        boolean result = false;

        if (this.findById(id).isPresent()==true){
            System.out.println("Producto a borrar encontrado");
            this.productRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
