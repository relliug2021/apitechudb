package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.Result;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    public List<PurchaseModel> findAll(){
        System.out.println("findAll en ProductService");
        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById");
        return this.purchaseRepository.findById(id);
    }

    public Result addPurchase(PurchaseModel purchase){

        Optional userFind = userService.findById(purchase.getUserId());
        if (userFind.isPresent()){
            Optional existPurchaseId = findById(purchase.getId());
            if (existPurchaseId.isEmpty()){
                AtomicReference<Boolean> errorIdProducts = new AtomicReference<>(false);
                AtomicReference<Float> totalAmount = new AtomicReference<>(0f);
                purchase.getPurchaseItems().forEach((p,n) -> {
                   Optional product = productService.findById(p);
                   if (product.isEmpty()){
                       errorIdProducts.set(true);
                       System.out.println("El siguiente producto no existe: " + p);
                   }else{
                       ProductModel productoObject = (ProductModel) product.get();
                       totalAmount.updateAndGet(v -> v + (productoObject.getPrice() * n));
                   }
                });
                if (errorIdProducts.get()==false){
                    purchase.setAmount(totalAmount.get());
                    return new Result("1", purchaseRepository.save(purchase), HttpStatus.OK);
                }else{
                    System.out.println("HAY ALGÚN PRODUCTO NO IDENTFICADO");
                    return new Result("HAY ALGÚN PRODUCTO NO IDENTFICADO",new PurchaseModel(), HttpStatus.NOT_FOUND);
                }
            }else{
                System.out.println("Error. El id de la compra ya existe");
                return new Result("Error. El id de la compra ya existe",new PurchaseModel(), HttpStatus.BAD_REQUEST);
            }

        }else{
            System.out.println("Usuario no encontrado");
            return new Result("Usuario no encontrado",new PurchaseModel(), HttpStatus.NOT_FOUND);
        }

    }
}
