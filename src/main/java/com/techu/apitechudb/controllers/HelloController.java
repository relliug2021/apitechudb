package com.techu.apitechudb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController //por defecto el controlador cuando devuelves algo es a través de vistas (para visualizarse en los navegadores). Pero en este caso, al solo tener la parte back, lo ponemos para que devuelva directametne el json. Si sería un jsp o html, sería @Controller

public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "Hola mundo desde API Tech U!";
    }

    @RequestMapping("/hello") //Para enlanzar la url /hello con esta clase
    public String hello(
            @RequestParam(value="name", defaultValue="Tech U") String name){
        return String.format("Hola %s!",name);
    }
}
