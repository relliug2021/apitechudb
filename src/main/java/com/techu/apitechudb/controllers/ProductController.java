package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.StyledEditorKit;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2") //es una forma de poner por defecto este inicio en todas las rutas de la clase
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProduct(){
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("Buscando el producto" + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "Producto no encontrado",
                                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){ //ResponseEntity es el objeto de comunicación con el cliente que utiliza spring
        System.out.println("addProduct");

        return new ResponseEntity<>(this.productService.addProduct(product),HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product,@PathVariable String id){
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La descrip del producto a actualizar " + product.getDesc());
        System.out.println("El precio del producto a actualizar " + product.getPrice());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productService.updateProduct(product);
        }
        return  new ResponseEntity<>(product, productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){

        Optional<ProductModel> productToDelete = this.productService.findById(id);
        Boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "producto borrado" : "no se ha podido borrar",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
