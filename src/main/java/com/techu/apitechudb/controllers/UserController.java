package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value="$orderby", required = false) String sort_type){
        System.out.println("getUsers con parametro: " + sort_type);
        List<UserModel> sortUsers;

        if (sort_type != null){
            System.out.println("LA LISTA DE USUARIOS SE VA A ORDENAR POR " + sort_type);
            sortUsers = this.userService.findAllSorted(sort_type);
        }else{
            sortUsers = this.userService.findAll();
        }
        return new ResponseEntity<>(sortUsers, HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("Buscando el User " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){ //ResponseEntity es el objeto de comunicación con el cliente que utiliza spring
        System.out.println("addUser");

        return new ResponseEntity<>(this.userService.addUser(user),HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user,@PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del User a actualizar es " + id);
        System.out.println("Name del User a actualizar " + user.getName());
        System.out.println("Age del User a actualizar " + user.getAge());

        Optional<UserModel> UserToUpdate = this.userService.findById(id);

        if (UserToUpdate.isPresent()){
            System.out.println("User para actualizar encontrado, actualizando");
            this.userService.updateUser(user);
        }
        return  new ResponseEntity<>(user, UserToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){

        Optional<UserModel> userToDelete = this.userService.findById(id);
        Boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usero borrado" : "no se ha podido borrar",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
