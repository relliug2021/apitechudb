package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.Result;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchasesById(@PathVariable String id){
        System.out.println("getPurchasesById");

        Optional<PurchaseModel> result = this.purchaseService.findById(id);
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Purchase no encontrado",
                result.isPresent() ?HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> createPurchase(@RequestBody PurchaseModel purchase){
        System.out.println("createPurchase");
        System.out.println("ID: " + purchase.getId());
        System.out.println("ID USER: " + purchase.getUserId());
        System.out.println("Amount: " + purchase.getAmount());
        System.out.println("List of products: "); purchase.getPurchaseItems().forEach((p,a) -> System.out.println(p));

        Result result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(((PurchaseModel)result.getObject()).isEmpty() ? result.getMessege() : result.getObject()
                , result.getStatus());
    }


}
