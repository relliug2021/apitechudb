package com.techu.apitechudb.models;

import org.springframework.http.HttpStatus;

public class Result {
    private String messege;
    private Object object;
    private HttpStatus status;

    public Result(String messege, Object object, HttpStatus status) {
        this.messege = messege;
        this.object = object;
        this.status = status;
    }

    public String getMessege() {
        return messege;
    }

    public void setMessege(String messege) {
        this.messege = messege;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
